//
//  ViewController.swift
//  letiLogin
//
//  Created by Анна Шамрикова on 14.10.2021.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var singInButoon: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setupButton(singInButoon)
    }
    
    private func setupButton(_ button: UIButton) {
        button.backgroundColor = .red
    }
}

